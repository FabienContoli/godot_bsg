extends Control


@onready var velocity :Label= $velocity
@onready var acceleration :Label= $acceleration
@onready var xRotation :Label= $xRotation
@onready var yRotation :Label= $yRotation
@onready var zRotation :Label= $zRotation
@onready var player :CharacterBody3D = get_tree().get_root().get_node("space_scene").get_node("world").get_node("player")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if player:
		velocity.text = "velocity:"+ str(player.velocity.length())
		acceleration.text ="acceleration:"+ str(player.acceleration)
		xRotation.text = "xRotation:"+str(player.xRotation)
		yRotation.text = "yRotation:"+str(player.yRotation)
		zRotation.text = "zRotation:"+str(player.zRotation)
