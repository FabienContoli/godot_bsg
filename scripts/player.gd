extends CharacterBody3D


@onready var ship:Node3D = $viperMk2
@onready var raycast:RayCast3D = $Camera3D/RayCast3D
const mouse_sensitivity:float = 0.01
const axisX = Vector3(1, 0, 0)
const axisY = Vector3(0, 1, 0)
const axisZ = Vector3(0, 0, 1)

@export var ROTATION_SPEED: float =2.0
@export var ENGINE_POWER: float =100.0
@export var FRICTION_FACTOR: float = -0.5
@export var MAX_ROTATION: float = 2
@export var MAX_ACCELERATION: float = 500

var acceleration = 0.0
var xRotation:float = 0.0;
var zRotation:float = 0.0;
var yRotation: float = 0.0;
func _unhandled_input(event: InputEvent)-> void:
	print(event)
	if event is InputEventMouseButton:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	elif event.is_action_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event is InputEventMouseMotion:
			pass
			#rotate_y(-event.relative.x * mouse_sensitivity)
			#neck.rotate_x(-event.relative.y * mouse_sensitivity)
			#neck.rotation.x = clamp(neck.rotation.x,deg_to_rad(-40),deg_to_rad(70))

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
		
	var input_dir_roll = Input.get_axis("roll_left","roll_right")
	var input_dir = Input.get_vector("right","left","down","up")
	
	if input_dir.y != 0:
		xRotation  += input_dir.y * delta  * ROTATION_SPEED
	else:
		xRotation =lerp(xRotation, 0.0, ROTATION_SPEED *delta)
	
	if input_dir.x != 0:
		yRotation  += input_dir.x * delta * ROTATION_SPEED
	else:
		yRotation =lerp(yRotation, 0.0, ROTATION_SPEED *delta)

	if input_dir_roll != 0:
		zRotation += input_dir_roll *delta * ROTATION_SPEED
	else:
		zRotation = lerp(zRotation, 0.0,ROTATION_SPEED *delta)
	
	
	velocity += velocity  * FRICTION_FACTOR * delta
	var boost = 1
	if Input.is_action_pressed("boost") :
		boost=10
	
	if Input.is_action_pressed("accelerate") :
		var strength = Input.get_action_strength("accelerate")
		acceleration+= ENGINE_POWER *delta * boost *strength
		print(strength)
		acceleration=clampf(acceleration,0.0,MAX_ACCELERATION);
		velocity += -global_transform.basis.z * acceleration * delta
		if ship.has_method("accelerate"):
			ship.accelerate(true,acceleration,delta)
	else:
		acceleration= lerp(acceleration,0.0,1.0 * delta)
		velocity += velocity  * FRICTION_FACTOR * delta
		if ship.has_method("accelerate"):
			ship.accelerate(false,acceleration,delta)
	
	xRotation= clampf(xRotation,-MAX_ROTATION,MAX_ROTATION)
	yRotation= clampf(yRotation,-MAX_ROTATION,MAX_ROTATION)
	zRotation= clampf(zRotation,-MAX_ROTATION,MAX_ROTATION)
	rotate_object_local(axisZ,zRotation * delta)
	rotate_object_local(axisX,xRotation * delta)
	rotate_object_local(axisY,yRotation * delta)
	
	ship.rotation.x = xRotation*(MAX_ROTATION/10)
	ship.rotation.z = yRotation*(MAX_ROTATION/3.2)
	var has_collide = move_and_slide()
	var slide = get_slide_collision_count()
	if (has_collide && slide==0):
		acceleration=0.0
		velocity= Vector3.ZERO
	elif (has_collide && slide>0):
		acceleration-=((acceleration/2)*delta)
		velocity-=((velocity/2)*delta)
	
	if Input.is_action_pressed("shoot") :
		if ship.has_method("shoot"):
			var collisionPoint= raycast.get_collision_point()
			ship.shoot(velocity.length(),collisionPoint)

