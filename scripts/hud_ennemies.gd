extends TextureRect

const Util = preload("Utils.gd")

var _pointed_body = null
var _pointed_body_radius:float = 30.0
var _pointed_draw_radius:float = 8.0

signal look_at;

signal stop_look_at;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var camera := get_viewport().get_camera_3d()
	if camera == null:
		return
	
	if(Input.is_action_just_pressed("unwatch")):
		stop_look_at.emit();
		
	#var pointed_body: = _find_pointed_ennemy(camera)
	var pointed_body := _find_looked_ennemy(camera)
	if pointed_body == null:
		self.hide()
	else:
		var body_pos := pointed_body.global_transform.origin
		var right := camera.global_transform.basis.x
		#var body_edge_pos := body_pos + right * pointed_body.radius
		var body_edge_pos := body_pos + right * _pointed_draw_radius
		var screen_center := camera.unproject_position(body_pos)
		var screen_edge_pos := camera.unproject_position(body_edge_pos)
		var screen_radius : float = max(16.0, screen_center.distance_to(screen_edge_pos))
		
		if screen_radius > get_viewport().size.x * 0.5:
			self.hide()
		else:
			self.show()
			var camera_pos := camera.global_transform.origin
			var distance := body_pos.distance_to(camera_pos)
			var screen_radius_v := Vector2(screen_radius, screen_radius)
			var screen_top_left_pos := screen_center - screen_radius_v
				
			self.position = screen_top_left_pos
			self.size = 2.0 * screen_radius_v
			if(Input.is_action_just_pressed("watch")):
				look_at.emit(pointed_body)
	pass

func _find_pointed_ennemy(camera: Camera3D)-> Node3D:
	var cylons = get_tree().get_nodes_in_group('cylons')
	var camera_pos := camera.global_transform.origin
	var mouse_pos = get_viewport().get_mouse_position()
	var ray_origin = camera.project_ray_origin(mouse_pos)
	var ray_normal = camera.project_ray_normal(mouse_pos)
	var pointed_body = null
	var closest_distance_squared = -1.0
	for body in cylons:
		var body_pos = body.global_transform.origin
		#if Util.ray_intersects_sphere(ray_origin, ray_normal, body_pos, body.radius):
		if Util.ray_intersects_sphere(ray_origin, ray_normal, body_pos, _pointed_body_radius):
			var d = body_pos.distance_squared_to(camera_pos)
			if d < closest_distance_squared or closest_distance_squared < 0.0:
				pointed_body = body
				closest_distance_squared = d
	return pointed_body
	pass
	
func _find_looked_ennemy(camera: Camera3D)-> Node3D:
	var cylons = get_tree().get_nodes_in_group('cylons')
	var camera_pos := camera.global_transform.origin
	var center_pos =  get_viewport().get_visible_rect().get_center()
	var ray_origin = camera.project_ray_origin(center_pos)
	var ray_normal = camera.project_ray_normal(center_pos)
	var pointed_body = null
	var closest_distance_squared = -1.0
	for body in cylons:
		var body_pos = body.global_transform.origin
		#if Util.ray_intersects_sphere(ray_origin, ray_normal, body_pos, body.radius):
		if Util.ray_intersects_sphere(ray_origin, ray_normal, body_pos, _pointed_body_radius):
			var d = body_pos.distance_squared_to(camera_pos)
			if d < closest_distance_squared or closest_distance_squared < 0.0:
				pointed_body = body
				closest_distance_squared = d
	return pointed_body
	pass
