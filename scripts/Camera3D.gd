extends Camera3D

@export var lerp_speed = 100
@export var target_path : Node3D
@export var offset = Vector3(0, 1, 4)
@export var targetToFollow :CharacterBody3D
@export var targetToWatch :Node3D
@export var max_velocity:float = 800 
@export var min_fov:float = 60 
@export var max_fov:float = 75 
var custom_target_to_watch :Node3D;
# Called when the node enters the scene tree for the first time.
func _ready():
	
	var target_xform = target_path.global_transform.translated_local(offset)
	global_transform = target_xform
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !target_path:
		return
	
	if(custom_target_to_watch && custom_target_to_watch!=null):
		var direction = Vector3(targetToFollow.global_transform.origin-custom_target_to_watch.global_transform.origin).normalized()
		var target_xform = targetToFollow.global_transform.translated(direction*10 + offset)
		global_transform = global_transform.interpolate_with(target_xform, lerp_speed *delta )
		look_at(custom_target_to_watch.global_transform.origin ,targetToFollow.transform.basis.y)
	else:
		var target_xform = target_path.global_transform.translated_local(offset)
		#global_transform = global_transform.interpolate_with(target_xform, lerp_speed *delta )
		look_at(targetToFollow.global_transform.origin + (-targetToWatch.global_transform.basis.z * Vector3(1000.0,1000.0,1000.0)), targetToFollow.transform.basis.y)

	var new_fov= min_fov+(targetToFollow.velocity.length()/max_velocity)*(max_fov-min_fov)
	new_fov = clampf(fov,min_fov,max_fov)
	set_fov(fov)


func _on_texture_rect_2_look_at(node):
	custom_target_to_watch = node
	pass # Replace with function body.


func _on_texture_rect_2_stop_look_at():
	custom_target_to_watch=null;
	pass # Replace with function body.
