extends Node3D


@onready var particules:Node3D = $particles
@onready var audioEngine:AudioStreamPlayer3D = $AudioStreamPlayer3DEngine
@onready var audioGun:AudioStreamPlayer3D = $AudioStreamPlayer3DGun
@onready var tweenEndSound
@onready var is_accelerating = false
@onready var is_ready = false
@onready var Bullet = preload("res://scenes/weapons/bullet.tscn") 
@onready var BulletDecal = preload("res://scenes/weapons/bulletDecal.tscn") 
@onready var gunRight = $GunRight
@onready var gunLeft = $GunLeft
@onready var guns = [gunRight,gunLeft]
@onready var bullet: Node3D = Bullet.instantiate()
@onready var bulletDecal: Node3D = BulletDecal.instantiate()
@onready var raycast :RayCast3D= $RayCast3D

@export var MAX_ACCELERATION: float = 500.0
@export var MAX_PITCH_SCALE: float = 5
@export var MAX_VOLUME_DB: float = 1.0

var bulletIndex=0
var maxTime=0.1
var timeReload=0
var shot_speed=10
# Called when the node enters the scene tree for the first time.
func _ready():
	is_ready = true
	pass # Replace with function body.

func _process(delta):
	timeReload-=delta
	timeReload = max(timeReload,.0)
	
func shoot(speed :float,collisionPoint: Vector3):
	for index in len(guns):
		if index == bulletIndex && timeReload==0:
			audioGun.play(0)
			var target = raycast.get_collider()
			if target && target.has_method('hit'):
				target.hit()
				var b= bulletDecal.duplicate()
				target.add_child(b)
				b.global_transform.origin=raycast.get_collision_point()
				b.look_at(raycast.get_collision_point()+raycast.get_collision_normal(),Vector3.UP)
			var new_bullet: Node3D = bullet.duplicate()
			var gun = guns[index]
			gun.add_child(new_bullet)
			new_bullet.global_transform.origin = gun.global_transform.origin
			new_bullet.velocity_custom = new_bullet.global_transform.basis.z* -1*shot_speed * (1+(speed*0.005))
			new_bullet.top_level = true
			bulletIndex+=1
			timeReload=maxTime
	if bulletIndex>1: 
			bulletIndex=0
			
func end_sound():
	audioEngine.stop()
	
func accelerate(isAccelerating: bool,acceleration: float,delta):
	if(isAccelerating):
		if(tweenEndSound):
			tweenEndSound.stop()
		audioEngine.volume_db=0
		audioEngine.play(0)
		audioEngine.pitch_scale = lerpf(audioEngine.pitch_scale, 1 +(acceleration/MAX_ACCELERATION)*MAX_PITCH_SCALE , 1 * delta)
		audioEngine.volume_db = lerpf(audioEngine.volume_db, (acceleration/MAX_ACCELERATION)*MAX_VOLUME_DB , 1)
		is_accelerating=true
	else:
		if(is_accelerating):
			tweenEndSound = create_tween().set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_OUT)
			tweenEndSound.tween_property(audioEngine, "volume_db", -20, 1)
			tweenEndSound.tween_callback(end_sound)
			tweenEndSound.tween_interval(1.0)
		audioEngine.pitch_scale = lerpf(audioEngine.pitch_scale, 1.0 , 2.0 * delta)
		is_accelerating=false
	audioEngine.pitch_scale = clampf(audioEngine.pitch_scale,1,MAX_PITCH_SCALE)
	particules.visible=isAccelerating
